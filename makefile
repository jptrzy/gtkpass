.ONESHELL:

[DEFAULT]: run

run:
	mkdir -p build && \
	cd build && \
	meson setup && \
	meson compile && \
	./src/gtkpass

flatpak:
	flatpak build-init --type=app --arch=x86_64 ${HOME}/.var/app/org.gnome.Builder/cache/gnome-builder/projects/GtkPass/flatpak/staging/x86_64-main xyz.jptrzy.gtkpass org.gnome.Sdk org.gnome.Platform 43 --sdk-extension=org.freedesktop.Sdk.Extension.vala
	flatpak-builder --arch=x86_64 --ccache --force-clean --download-only --stop-at=gtkpass ${HOME}/.var/app/org.gnome.Builder/cache/gnome-builder/projects/GtkPass/flatpak/staging/x86_64-main xyz.jptrzy.gtkpass.json
	flatpak-builder --arch=x86_64 --ccache --force-clean --disable-updates --disable-download --stop-at=gtkpass ${HOME}/.var/app/org.gnome.Builder/cache/gnome-builder/projects/GtkPass/flatpak/staging/x86_64-main xyz.jptrzy.gtkpass.json
	





