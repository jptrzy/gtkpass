class PasswordEntry : GLib.Object {
    public string name {set;get;default = "NAME PLACEHOLDER";}
    public GLib.ListStore children_model {set;get;default = null;}
    public bool is_dir {get;set;default = true;}
}

namespace GtkPass {
    public string? get_pass_dir () {
        return Environment.get_variable ("HOME") + "/.local/share/pass";

        string pass_dir = Environment.get_variable ("PASSWORD_STORE_DIR");

        if (pass_dir == null || pass_dir == "") {
            pass_dir = Environment.get_variable ("HOME") + "/.password-store";
        }

        if (!GLib.FileUtils.test (pass_dir, GLib.FileTest.IS_DIR)) {
            warning ("Can't find password dir");
            return null;
        }

        return pass_dir;
    }

    public GLib.ListStore read_pass_dir (string dir_name) {
        var list_model = new GLib.ListStore(typeof (PasswordEntry));

        var dir = Dir.open (dir_name);
        string? name = null;
        while ((name = dir.read_name ()) != null) {
            string path = Path.build_filename (dir_name, name);
            var entry = new PasswordEntry () {};
            entry.name = name;
            if (GLib.FileUtils.test (path, FileTest.IS_REGULAR)) {
                //TODO Simplify
                if (name.substring(name.index_of_nth_char(name.length - 4), 4) == ".gpg") {
                    entry.name = name.substring(0, name.length - 4);

                    list_model.append (entry);
                }
            } else if (GLib.FileUtils.test (path, FileTest.IS_DIR)) {
                entry.children_model = read_pass_dir (path);
                list_model.append (entry);
            }
        }

        return list_model;
    }

    [GtkTemplate (ui = "/xyz/jptrzy/gtkpass/window.ui")]
    public class Window : Adw.ApplicationWindow {
        [GtkChild] private unowned Gtk.ListView passwords_list;
        [GtkChild] private unowned Gtk.SearchBar search_bar;
        [GtkChild] private unowned Gtk.SearchEntry search_entry;

        private static string search_text = "";

        private Gtk.CustomFilter filter 
            = new Gtk.CustomFilter (password_entry_filter_by_name); 

        // demo_filter_by_name
        public static bool password_entry_filter_by_name (Object item) {
            Gtk.TreeListRow row = item as Gtk.TreeListRow;

            if (search_text == "") return true;

            for (Gtk.TreeListRow? parent = row; parent != null; parent = parent.get_parent ()) {
                var password_entry = parent.item as PasswordEntry;

                if (password_entry != null && search_text.match_string (password_entry.name, true)) {
                    return true;
                }
            }

            var list_model = row.children;

            if (list_model == null) return false;

            for (int i=0; i < list_model.get_n_items (); i++) {
                 var password_entry = list_model.get_object (i) as PasswordEntry;
                if (password_entry != null && search_text.match_string (password_entry.name, true)) {
                    return true;
                }
            }

            return false;
        }

        public Window (Gtk.Application app) {
            Object (application: app);

            var dir_name = get_pass_dir ();
            var list_model = read_pass_dir (dir_name);

            var tree_model = new Gtk.TreeListModel(
                list_model, false, true, (entry) => {
                    return ((PasswordEntry) entry).children_model;
                }
            );

            var filter_model = new Gtk.FilterListModel (tree_model, filter);

            passwords_list.set_model (new Gtk.NoSelection (filter_model));

            search_bar.set_key_capture_widget (this);
        }

        [GtkCallback]
        public void on_search_entry_change () {
            bool strict = search_entry.text.length > search_text.length;
            search_text = search_entry.text;
            filter.changed (strict ? Gtk.FilterChange.MORE_STRICT : Gtk.FilterChange.LESS_STRICT);
        }
    }
}
